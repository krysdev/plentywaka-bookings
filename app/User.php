<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'address',
        'phone_number',
        'company_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
      return $this->belongsTo(Roles::class);
    }
    public function company()
    {
      return $this->hasMany(Company::class);
    }
    public function package()
    {
      return $this->hasMany(Packages::class);
    }
    public function inventory()
    {
      return $this->hasMany(Inventory::class);
    }
    public function trip()
    {
      return $this->hasMany(Trip::class);
    }
    public function staff()
    {
      return $this->hasMany(Staff::class);
    }

    public function isAdmin(){
      if($this->role->name == "user"){
        return false;
      }

      return true;
    }
}
