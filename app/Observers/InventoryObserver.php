<?php

namespace App\Observers;

use App\Inventory;
use Illuminate\Support\Facades\Auth;

class InventoryObserver
{
    /**
     * Handle the inventory "created" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function creating(Inventory $inventory)
    {
        $inventory->user_id = Auth::user()->id;
        
    }

    public function created(Inventory $inventory)
    {
        //
    }

    /**
     * Handle the inventory "updated" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function updated(Inventory $inventory)
    {
        //
    }

    /**
     * Handle the inventory "deleted" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function deleted(Inventory $inventory)
    {
        //
    }

    /**
     * Handle the inventory "restored" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function restored(Inventory $inventory)
    {
        //
    }

    /**
     * Handle the inventory "force deleted" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function forceDeleted(Inventory $inventory)
    {
        //
    }
}
