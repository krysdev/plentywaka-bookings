<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Staff;
use App\Ride;
use App\Trip;
use App\Inventory;
use Validator;
use Carbon\Carbon;

class ApiController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('auth.apikey');
    }

    public function allUsers()
    {
      $users = User::all();
      return response()->json(['data' => $users], 200);
    }

    public function getCompanyById($id)
    {
      $user = User::find($id);
      if($user){
        return response()->json(['data' => $user], 200);
      }else{
        return response()->json(['message' => 'Company does not exist!'], 404);
      }
      
    }

    public function getStaffRidesByPhone($num)
    {
      $staff = Staff::where('phone_number', $num)->first();
      if($staff){
        $getStaffRides = Ride::where('staff_id', $staff->id)->get();
        return response()->json(['data' => $getStaffRides], 200);
      }else{
        return response()->json(['message' => 'Staff not found!'], 404);
      }
      
    }

    
  public function staffLogin(Request $request)
  {
    $staff = Staff::where('staff_pin', $request->input('staff-pin'))->first();
    if($staff){
      $companyCode = $staff->user->company_code;
      if($request->input('company-code') == $companyCode){
        return response()->json(['data' => $staff, 'message' => 'login successful'], 200);
      }else{
        return response()->json(['message' => 'Incorrect Company code!'], 403);
      }
    }else{
      return response()->json(['message' => 'Staff does not exist!'], 404);
    }
  }
  // public function staffRides(Request $request)
  // {
  //   $validator = Validator::make($request->all(), [
  //     'staff-id' => 'required|integer',
  //     'trip-id' => 'required|integer',
  //     'pickup-point' => 'required|string',
  //     'dropoff-point' => 'required|string',
  //   ]);

  //   if ($validator->fails()) {
  //     return response()->json(['message' => $validator->errors()], 400);
  //   }else{
  //     $staffRide = Ride::create([
  //       'staff_id' => $request->input('staff-id'),
  //       'trip_id' => $request->input('trip-id'),
  //       'pickup_point' => $request->input('pickup-point'),
  //       'dropoff_point' => $request->input('dropoff-point'),
  //       'checkin_time' => Carbon::now(),
  
  //     ]);

  //     if ($staffRide) {
  //       return response()->json(['data' => $staffRide, 'message' => 'successful'], 200);
  //     }else{
  //       return response()->json(['message' => 'Not successful. Please check your internet connection.'], 500);
  //     }
  //   }

  // }

  // public function completeRide(Request $request, $rideId){
  //   if($rideId){
  //     $ride_exists = Ride::findorFail($rideId);
  //     if($ride_exists){
  //       $ride_exists->update([
  //         'checkout_time' => Carbon::now(),
  //     ]);
  //     return response()->json(['message' => 'Ride completed successfully'], 200);
  //     }else{
  //       return response()->json(['message' => 'Not successful. Please check your internet connection.'], 500);
  //     }
  //   }else{
  //     return response()->json(['message' => 'No ride id.'],  404);
  //   }
  // }

  public function vALogin(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'company-code' => 'required',
      'bus-number' => 'required',
    ]);
    if ($validator->fails()) {
      return response()->json(['message' => $validator->errors()], 400);
    }else{
      $getCompanyCode = User::where('company_code', $request->input('company-code'))->first();
      if(empty($getCompanyCode)){
        return response()->json(['message' => 'Company does not exist!'], 404);
      }
      $getBusId = Inventory::where('bus_number', $request->input('bus-number'))->first();

      if(empty($getBusId)){
        return response()->json(['message' => 'Bus does not exist!'], 404);
      }

      if($getCompanyCode && $getBusId){
        $va_login = Trip::where('user_id', $getCompanyCode->id)->where('inventory_id', $getBusId->id)->latest()->first();
        if($va_login){
            return response()->json(['data' => $va_login, 'message' => 'login successful'], 200);
         
        }else{
          return response()->json(['message' => 'Trip does not exist!'], 404);
        }
      }else{
        return response()->json(['message' => 'Not successful. Please check your internet connection.'], 500);
      }
    }
  }

  public function checkinStaff(Request $request){
    $validator = Validator::make($request->all(), [
      'phone-number' => 'required',
      'bus-number' => 'required',
      'trip-id' => 'required',
    ]);
    if ($validator->fails()) {
      return response()->json(['message' => $validator->errors()], 400);
    }else{
      $staff = Staff::where('phone_number', $request->input('phone-number'))->first();
      if(empty($staff)){
        return response()->json(['message' => 'Staff does not exist!'], 404);
      }
      $getBus = Inventory::where('bus_number', $request->input('bus-number'))->first();
      if(empty($getBus)){
        return response()->json(['message' => 'Bus does not exist!'], 404);
      }
      $getTrip = Trip::find($request->input('trip-id'));
      if(empty($getTrip)){
        return response()->json(['message' => 'Trip does not exist!'], 404);
      }
        $staffRide = Ride::create([
        'staff_id' => $staff->id,
        'trip_id' => $request->input('trip-id'),
        'checkin_time' => Carbon::now(),
  
      ]);

        if($staffRide) {
          return response()->json(['data' => $staffRide, 'message' => 'successful'], 200);
        }else{
          return response()->json(['message' => 'Not successful. Please check your internet connection.'], 500);
        }

    }
  }

  public function checkoutStaff(Request $request){
    $validator = Validator::make($request->all(), [
      'phone-number' => 'required',
      'bus-number' => 'required',
      'trip-id' => 'required',
    ]);
    if ($validator->fails()) {
      return response()->json(['message' => $validator->errors()], 400);
    }else{
      $staff = Staff::where('phone_number', $request->input('phone-number'))->first();
      if(empty($staff)){
        return response()->json(['message' => 'Staff does not exist!'], 404);
      }
      $getBus = Inventory::where('bus_number', $request->input('bus-number'))->first();
      if(empty($getBus)){
        return response()->json(['message' => 'Bus does not exist!'], 404);
      }
      $getTrip = Trip::find($request->input('trip-id'));
      if(empty($getTrip)){
        return response()->json(['message' => 'Trip does not exist!'], 404);
      }
      
      $ride_exists = Ride::where('staff_id', $staff->id)->where('trip_id', $request->input('trip-id'))->first();
      if($ride_exists){
        $ride_exists->update([
          'checkout_time' => Carbon::now(),
      ]);
      return response()->json(['message' => 'Staff checked out successfully'], 200);
      }else{
        return response()->json(['message' => 'Not successful. Ride does not exit or network failure.'], 500);
      }

    }
  }

}
