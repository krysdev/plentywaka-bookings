<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    //
    protected $fillable = [
        'user_id','name','bus_stop','staff_pin','phone_number',
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
    public function ride()
    {
      return $this->hasMany(Ride::class);
    }
}
