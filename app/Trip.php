<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    //
    protected $fillable = [
        'user_id',
        'inventory_id',
        'company_id',
        'start_location',
        'end_location',
        'trip_date',
    ];

    protected $casts = [
        'trip_date' => 'datetime'
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function inventory()
    {
      return $this->belongsTo(Inventory::class);
    }

    public function company()
    {
      return $this->belongsTo(Company::class);
    }
    public function ride()
    {
      return $this->hasMany(Ride::class);
    }
    public function package()
    {
      return $this->belongsTo(Package::class);
    }
}
