<?php

namespace App\Observers;

use App\Package;
use Illuminate\Support\Facades\Auth;

class PackageObserver
{
    /**
     * Handle the packages "created" event.
     *
     * @param  \App\Package  $packages
     * @return void
     */
    public function creating(Package $package)
    {
        $package->user_id = Auth::user()->id;
    }

    public function created(Package $package)
    {
        //
    }

    /**
     * Handle the packages "updated" event.
     *
     * @param  \App\Package  $packages
     * @return void
     */
    public function updated(Package $package)
    {
        //
    }

    /**
     * Handle the packages "deleted" event.
     *
     * @param  \App\Package  $packages
     * @return void
     */
    public function deleted(Package $package)
    {
        //
    }

    /**
     * Handle the packages "restored" event.
     *
     * @param  \App\Package  $packages
     * @return void
     */
    public function restored(Package $package)
    {
        //
    }

    /**
     * Handle the packages "force deleted" event.
     *
     * @param  \App\Package  $packages
     * @return void
     */
    public function forceDeleted(Package $package)
    {
        //
    }
}
