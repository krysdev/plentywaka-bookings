<?php

namespace App\Policies;

use App\User;
use App\Ride;
use Illuminate\Auth\Access\HandlesAuthorization;

class RidePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any rides.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        return true;
    }

    /**
     * Determine whether the user can view the ride.
     *
     * @param  \App\User  $user
     * @param  \App\Ride  $ride
     * @return mixed
     */
    public function view(User $user, Ride $ride)
    {
        if($user->isAdmin()){
            return true;
        }
        else if($ride->staff->user_id == $user->id){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can create rides.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the ride.
     *
     * @param  \App\User  $user
     * @param  \App\Ride  $ride
     * @return mixed
     */
    public function update(User $user, Ride $ride)
    {
        //
    }

    /**
     * Determine whether the user can delete the ride.
     *
     * @param  \App\User  $user
     * @param  \App\Ride  $ride
     * @return mixed
     */
    public function delete(User $user, Ride $ride)
    {
        //
    }

    /**
     * Determine whether the user can restore the ride.
     *
     * @param  \App\User  $user
     * @param  \App\Ride  $ride
     * @return mixed
     */
    public function restore(User $user, Ride $ride)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the ride.
     *
     * @param  \App\User  $user
     * @param  \App\Ride  $ride
     * @return mixed
     */
    public function forceDelete(User $user, Ride $ride)
    {
        //
    }
}
