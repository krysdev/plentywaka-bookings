<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\DateTime;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Trip extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Trip';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'user_id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Bus Number', 'inventory', 'App\Nova\Inventory')->rules('required'),
            BelongsTo::make('Company', 'user', 'App\Nova\User')->sortable()->rules('required'),
            BelongsTo::make('Package')->rules('required'),
            Place::make('Start Location')->countries(['NG'])->sortable()->rules('required'),
            Place::make('End Location')->countries(['NG'])->sortable()->rules('required'),
            DateTime::make('Trip Date')->sortable()->rules('required'),
            // Text::make('Trip Date', function () {
            //     //$tripTime = Carbon::createFromFormat('Y-m-d h:i a', $this->trip_date);
            //     return Carbon::parse($this->trip_date, 'WAT')->format('Y-m-d h:i a');

            // }),    
            
        ];
    }

    public static function indexQuery(NovaRequest $request, $query){
        $user = Auth::user();
        if($user->isAdmin()){
            return $query;
        }else{
            return $query->where('user_id', $user->id);
        }
        
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
