<?php

namespace App\Observers;

use App\Trip;
use Illuminate\Support\Facades\Auth;

class TripObserver
{
    /**
     * Handle the trip "created" event.
     *
     * @param  \App\Trip  $trip
     * @return void
     */
    public function creating(Trip $trip)
    {
        //$trip->user_id = Auth::user()->id;
    }

    public function created(Trip $trip)
    {
        //
    }

    /**
     * Handle the trip "updated" event.
     *
     * @param  \App\Trip  $trip
     * @return void
     */
    public function updated(Trip $trip)
    {
        //
    }

    /**
     * Handle the trip "deleted" event.
     *
     * @param  \App\Trip  $trip
     * @return void
     */
    public function deleted(Trip $trip)
    {
        //
    }

    /**
     * Handle the trip "restored" event.
     *
     * @param  \App\Trip  $trip
     * @return void
     */
    public function restored(Trip $trip)
    {
        //
    }

    /**
     * Handle the trip "force deleted" event.
     *
     * @param  \App\Trip  $trip
     * @return void
     */
    public function forceDeleted(Trip $trip)
    {
        //
    }
}
