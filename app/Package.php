<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    protected $fillable = [
        'user_id','type',
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function trip()
    {
      return $this->hasMany(Trip::class);
    }

}
