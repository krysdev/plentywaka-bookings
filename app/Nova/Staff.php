<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Number;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Illuminate\Support\Facades\Auth;

class Staff extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Staff';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Name')->sortable()->rules('required'),
            Place::make('Bus Stop')->countries(['NG'])->sortable()->rules('required'),
            Number::make('Staff Pin')->exceptOnForms(),
            Text::make('Phone Number')->sortable()->rules('required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10')->nullable(),
            BelongsTo::make('Company', 'user')->exceptOnForms(),
        ];
    }

    public static function indexQuery(NovaRequest $request, $query){
        $user = Auth::user();
        if($user->isAdmin()){
            return $query;
        }else{
            return $query->where('user_id', $user->id);
        }
        
    }

    public static function availableForNavigation(Request $request)
    {
        if(Auth::user()->isAdmin()){
            return false;
        }
        return true;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
