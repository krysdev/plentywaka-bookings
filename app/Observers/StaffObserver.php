<?php

namespace App\Observers;

use App\Staff;
use Illuminate\Support\Facades\Auth;

class StaffObserver
{
    /**
     * Handle the staff "created" event.
     *
     * @param  \App\Staff  $staff
     * @return void
     */
    public function creating(Staff $staff)
    {
        if(!Auth::user()->isAdmin()){
            $staff->user_id = Auth::user()->id;
        }
        
        $staff->staff_pin = mt_rand(1000, 9999);
    }

    public function created(Staff $staff)
    {
        //
    }

    /**
     * Handle the staff "updated" event.
     *
     * @param  \App\Staff  $staff
     * @return void
     */
    public function updated(Staff $staff)
    {
        //
    }

    /**
     * Handle the staff "deleted" event.
     *
     * @param  \App\Staff  $staff
     * @return void
     */
    public function deleted(Staff $staff)
    {
        //
    }

    /**
     * Handle the staff "restored" event.
     *
     * @param  \App\Staff  $staff
     * @return void
     */
    public function restored(Staff $staff)
    {
        //
    }

    /**
     * Handle the staff "force deleted" event.
     *
     * @param  \App\Staff  $staff
     * @return void
     */
    public function forceDeleted(Staff $staff)
    {
        //
    }
}
