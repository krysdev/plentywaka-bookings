<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Observers\CompanyObserver;
use App\Observers\PackageObserver;
use App\Observers\InventoryObserver;
use App\Observers\TripObserver;
use App\Observers\UserObserver;
use App\Observers\StaffObserver;
use App\Company;
use App\Package;
use App\Inventory;
use App\Trip;
use App\User;
use App\Staff;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        Company::observe(CompanyObserver::class);
        Package::observe(PackageObserver::class);
        Inventory::observe(InventoryObserver::class);
        Trip::observe(TripObserver::class);
        User::observe(UserObserver::class);
        Staff::observe(StaffObserver::class);
    }
}
