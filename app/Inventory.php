<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    //
    protected $fillable = [
        'user_id','bus_number','plate_number',
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
    public function trip()
    {
      return $this->hasMany(Trip::class);
    }
}
