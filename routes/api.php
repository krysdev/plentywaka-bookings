<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('users', 'ApiController@allUsers');
Route::get('company/{id}', 'ApiController@getCompanyById');
Route::get('staff-rides/{phone}', 'ApiController@getStaffRidesByPhone');
Route::post('staff-login', 'ApiController@staffLogin');
Route::post('checkin-staff', 'ApiController@checkinStaff');
Route::post('checkout-staff', 'ApiController@checkoutStaff');
Route::post('va-login', 'ApiController@vALogin');
