<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    //
    protected $fillable = [
        'staff_id',
        'trip_id',
        'checkin_time',
        'checkout_time',
    ];

    public function staff()
    {
      return $this->belongsTo(Staff::class);
    }
    public function trip()
    {
      return $this->belongsTo(Trip::class);
    }
}
