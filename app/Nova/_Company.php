<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Place;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Illuminate\Support\Facades\Auth;


class Company extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Company';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';


    public function subtitle(){
        return $this->address;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('User')->exceptOnForms(),
            Text::make('Name')->sortable()->rules('required'),
            Place::make('Address', 'address')->countries(['NG'])->sortable()->rules('required'),
            Text::make('Phone Number')->sortable()->rules('required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10')->nullable(),
            Text::make('Company Code')->exceptOnForms(),
        ];
    }
    // $this->addressFields(),
//     protected function addressFields()
// {
//     return $this->merge([
//         Place::make('Address', 'address_line_1')->hideFromIndex(),
//         Text::make('Address Line 2')->hideFromIndex(),
//         Text::make('City')->hideFromIndex(),
//         Text::make('State')->hideFromIndex(),
//         Text::make('Postal Code')->hideFromIndex(),
//         Text::make('Suburb')->hideFromIndex(),
//         Country::make('Country')->hideFromIndex(),
//         Text::make('Latitude')->hideFromIndex(),
//         Text::make('Longitude')->hideFromIndex(),
//     ]);
// }

public static function availableForNavigation(Request $request)
{
    return Auth::user()->isAdmin();
}

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
