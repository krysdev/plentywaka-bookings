<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $fillable = [
        'user_id','name','address','phone_number','company_code',
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
    public function trip()
    {
      return $this->hasMany(Trip::class);
    }
}
