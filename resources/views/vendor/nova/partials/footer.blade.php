<p class="mt-8 text-center text-xs text-80">
    <a href="https://nova.laravel.com" class="text-primary dim no-underline">Bookings Admin</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} PlentyWaka Bookings - By PlentyWaka.
    <span class="px-1">&middot;</span>
    v{{ \Laravel\Nova\Nova::version() }}
</p>
