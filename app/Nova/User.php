<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;

use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Http\Requests\NovaRequest;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\User';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    // public function title()
    // {
    //     return $this->name. ' '. $this->name;
    // }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'email',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Company Name', 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Email')
                ->sortable()
                ->rules('nullable','email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('nullable','string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),
            Place::make('Address')->countries(['NG'])->sortable()->rules('required')->nullable(),
            Text::make('Phone Number')->sortable()->rules('regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10')->nullable(),
            Text::make('Company Code')->exceptOnForms(),    

            BelongsTo::make('Role')
                ->showOnIndex(function () {
                    return Auth::user()->isAdmin();
                })
                ->showOnDetail(function () {
                    return Auth::user()->isAdmin();
                })
                ->showOnCreating(function () {
                    return Auth::user()->isAdmin();
                })
                ->showOnUpdating(function () {
                    return Auth::user()->isAdmin();
                }), 
            HasMany::make('Staff'), 
        ];
    }

    public static function label()
    {
        if(Auth::user()->isAdmin()){
            return 'Companies';
        }else {
            return 'My Profile';
        }
        
    }
    public static function indexQuery(NovaRequest $request, $query){
        $user = Auth::user();
        if($user->isAdmin()){
            return $query;
        }else{
            return $query->where('id', $user->id);
        }
    }

    // public static function availableForNavigation(Request $request)
    // {
    //     return Auth::user()->isAdmin();
    // }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
